/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.usermanamentproject;

import java.util.ArrayList;

/**
 *
 * @author MSiRTX
 */
public class UserService {

    private ArrayList<User> userlist;
    private int lastId = 1;

    public UserService() {
        userlist = new ArrayList<User>();
    }

    public User addUser(User newUser) {
        newUser.setId(lastId++);
        userlist.add(newUser);
        return newUser;
    }

    public User getUser(int index) {
        return userlist.get(index);
    }
    public ArrayList<User> getUsers() {
        return userlist;
    }
    public int getSize() {
        return userlist.size();
    }
    public void logUserList(){
        for(User u : userlist){
            System.out.println(u);
        }
    }

    User update(int index, User updateUser) {
        User user = userlist.get(index);
        user.setLogin(updateUser.getLogin());
        user.setName(updateUser.getName());
        user.setPassword(updateUser.getPassword());
        user.setGender(updateUser.getGender());
        user.setRole(updateUser.getRole());
        return user;
    }

    User deleteUser(int index) {
        return userlist.remove(index);
    }

}
